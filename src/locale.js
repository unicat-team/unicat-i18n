const i18n = require("i18n");

export default (locale, directory = `${__dirname}/locales`) => {
    const t = {};
    
    i18n.configure({
      locales:['en', 'ru'],
      updateFiles: false,
      directory,
      register: t,
    });
    
    t.setLocale(locale);
    return t.__n;
}
  