import { safeLoad } from 'js-yaml';
import { readdirSync, readFileSync, writeFileSync } from 'fs';

export default (from, to) => {
  readdirSync(from)
    .filter(file => file.endsWith('.yml'))
    .map(file => ({
      lang: file.split('.').find((s, i, a) => i === a.length - 2),
      json: safeLoad(readFileSync(`${from}/${file}`)) }))
    .reduce((acc, file) =>
      acc
        .filter(l => l.lang !== file.lang)
        .concat([{
          lang: file.lang,
          json: [file.json]
            .concat((acc.find(l => l.lang === file.lang) || { json: [] }).json),
        }])
      , [])
    .map(locale =>
      ({
        lang: locale.lang,
        json: locale.json.reduce((acc, o) => Object.assign(acc, o), {}) }))
    .forEach(locale =>
      writeFileSync(`${to}/${locale.lang}.json`, JSON.stringify(locale.json, null, 2)));
};

