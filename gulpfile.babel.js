import gulp from 'gulp';
import {mkdirSync} from 'fs';
import converter from './src/converter';

gulp.task('compile-locales', () => {
  mkdirSync('dist/locales');
  converter('src/locales', 'dist/locales');
});
