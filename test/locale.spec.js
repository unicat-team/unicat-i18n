import {assert} from 'chai';
import locale from '../src/locale';
 
describe('i18n', () => {
   it('translate', () => {
    const t = locale('ru', __dirname + '/fixtures');
    
    assert.equal(t('Product Name'), 'Наименование');
  });
});
