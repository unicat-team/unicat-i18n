import convert from '../src/converter';
import { readdirSync, unlinkSync } from 'fs';

const localesDir = `${__dirname}/fixtures`;

before(() => 
  readdirSync(`${localesDir}`)
    .filter(file => file.endsWith('.json'))
    .forEach(file => unlinkSync(`${localesDir}/${file}`))
);


describe('converter', () => {
  it('convert yml -> json', () => {
    convert(localesDir, localesDir);
  });
});
